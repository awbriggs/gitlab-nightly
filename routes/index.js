const https = require('https');
const express = require('express');
const router = express.Router();

const SECONDS_IN_MINUTE = 60;
const MINUTES_IN_HOUR = 60;
const HOURS_IN_DAY = 24;
const MILLISECONDS_IN_DAY = HOURS_IN_DAY * MINUTES_IN_HOUR * SECONDS_IN_MINUTE * 1000;


const RELEASE_VERSIONS = {
	"nightly": {
		url: "",
		version: "",
		timeLastGot: 0
	},
	"current": {
		url: "",
		version: "",
		timeLastGot: 0
	},
	"release-candidate": {
		url: "",
		version: "",
		timeLastGot: 0
	},
	"release-nightly": {
		url: "",
		version: "",
		timeLastGot: 0
	},
}

router.get('/:versionType', (req, res, next) => {
	const versionType = req.params['versionType'].toLowerCase()
	if (RELEASE_VERSIONS.hasOwnProperty(versionType)) {
		res.redirect(getReleaseUrl(versionType));
	} else {
		res.status(400);
	}

	next();
});

function getReleaseUrl(versionType) {
	const currentDate = Date.now();
	if ((currentDate - RELEASE_VERSIONS[versionType].timeLastGot) > MILLISECONDS_IN_DAY) {
		refreshCurrentData(versionType);
	}

	return RELEASE_VERSIONS[versionType].url;
}

function refreshCurrentData(versionType) {
	https.get(`https://services.gradle.org/versions/${versionType}`, (res) => {
		let data = '';

		res.on('data', (chunk => {
			data += chunk;
		}))

		res.on('end', () => {
			const response = JSON.parse(data);
			RELEASE_VERSIONS[versionType].url = response.downloadUrl;
			RELEASE_VERSIONS[versionType].version = response.version;
			RELEASE_VERSIONS[versionType].timeLastGot = Date.now();
		})
	}).on("error", (err) => {
		console.log("Error: " + err.message);
	})
}

module.exports = router;
